import { serveCoverage } from '../../utils';

/**
 * Executes the build process, serving unit test coverage report using an `express` services.
 */
export = serveCoverage;
