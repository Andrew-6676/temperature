import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule }     from '@angular/common';
import { FormsModule }      from '@angular/forms';
import { RouterModule }     from '@angular/router';

import { MaterialModule }   from '@angular/material';

import { ToolbarComponent }          from './components/toolbar/toolbar.component';
import { NavbarComponent }           from './components/navbar/navbar.component';
import { MessageComponent }          from './components/message/message.component';
import { ScrollingButtonsComponent } from './components/scrolling-buttons/scrolling-buttons.component';

/**
 * Do not specify providers for modules that might be imported by a lazy loaded module.
 */

@NgModule({
  imports: [CommonModule, RouterModule, MaterialModule],
  declarations: [
      ToolbarComponent,
      NavbarComponent,
      ScrollingButtonsComponent,
      MessageComponent,
  ],
  exports: [
      ToolbarComponent,
      NavbarComponent,
      CommonModule,
      FormsModule,
      RouterModule,
      MaterialModule,
      ScrollingButtonsComponent,
      MessageComponent,
  ]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: []
    };
  }
}
