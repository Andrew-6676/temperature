import { Injectable } from '@angular/core';

@Injectable()
export class LoggerService {
	enabled: boolean = true;

	constructor() {
		//this.debug('logger started: ');
	}

	stringify(obj:any) {
		return JSON.stringify(obj, null, '\t');
	}

	group()     { console.group(); }
	groupEnd()  { console.groupEnd(); }

	startTime(s:string) {
		console.time(s);
	}
	endTime(s:string) {
		console.timeEnd(s);
	}
	//console.info("%s numbers %d, %d and %d","hello",1,2,3);
	//console.log('%c Oh my heavens! ', 'background: #222; color: #bada55');

	debug(...msg: any[]) { console.debug(this.getTime()+': ', ...msg); }
	error(...msg: any[]) { console.error(this.getTime()+': ', ...msg); }
	warn(...msg: any[])  { console.warn(this.getTime()+': ', ...msg); }
	info(...msg: any[])  { console.info(this.getTime()+': ', ...msg); }
	log(...msg: any[])   { console.log(this.getTime()+': ', ...msg); }
	styledLog(str:string, style:string, msg: any = '')   { console.log(str, style, msg); }
	//test(...msg: any[])   { console.log(this.getTime()+': ', ...msg); }

    public getTime(ms=true) {
	    let d = new Date();
		return '['+d.getHours()+':'+(d.getMinutes()<10 ? '0' : '')+d.getMinutes()+':'+(d.getSeconds()<10 ? '0' : '')+d.getSeconds()+(ms ? '.'+d.getMilliseconds() : '')+']';
    }
}
