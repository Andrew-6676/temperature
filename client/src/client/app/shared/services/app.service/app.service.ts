import { Injectable }    from '@angular/core';
import { Http }          from '@angular/http';
import { MdSnackBar }    from '@angular/material';

import { Config }        from '../../config/env.config';
import { LoggerService } from '../logger.service/index';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';

/**
 * This class provides the NameList service with methods to read names and add names.
 */
@Injectable()
export class AppService {

	public baseURL = Config.API;

	app:  App  = new App();
	user: User = new User();

	redirectUrl: string = '';
	snackBarRef: any = null;
	// message: {
	// 	text: string,
	// 	color: string
	// }
	message: any = null;
	/*-----------------------------------------------------------------------------------------*/

	/**
	 * Creates a new NameListService with the injected Http.
	 * @param {Http} _http - The injected Http.
	 * @param {LoggerService} _logger - The injected LoggerService.
	 * @param {LoggerService} _snackbar - The injected MdSnackBar.
	 * @constructor
	 */
	constructor(
		private _http:Http,
	    private _logger:LoggerService,
		private _snackbar: MdSnackBar
	) {
		this._logger.log('Environment config', Config);
	}

	/*-----------------------------------------------------------------------------------------*/


	login(): Observable<boolean> {
		this.user.isGuest = false;
		return Observable.of(true); //.delay(1000).do((val:any) => this.user.isLoggedIn = true);
	}

	/*-----------------------------------------------------------------------------------------*/
	logout() {
		this.user.isGuest = true;
		this.user.isLoggedIn = false;
	}
	/*-----------------------------------------------------------------------------------------*/
	snackbarOpen(msg:string, duration?:number) {
		let d = new Date();
		let time = '['+d.getHours()+':'+(d.getMinutes()<10 ? '0' : '')+d.getMinutes()+':'+(d.getSeconds()<10 ? '0	' : '')+d.getSeconds()+'] ';
		// this._snackbar.open(time+' '+msg, 'OK', {duration:5000});
		let cfg = {};
		if (duration) {
			cfg = {duration: duration};
		}
		this.snackBarRef = this._snackbar.open(time+' '+msg, 'OK', cfg);
	}

	snackbarClose() {
		if (this.snackBarRef) {
			this.snackBarRef.dismiss();
			this.snackBarRef = null;
		}
	}
	/**
	 * Returns an Observable for the HTTP GET request for the JSON resource.
	 * @return {string[]} The Observable for the HTTP request.
	 */
	// get():Observable<string[]> {
	// 	return this._http.get('/assets/data.json')
	// 		.map((res:Response) => res.json())
	// 		.catch(this.handleError);
	// }

	/**
	 * Handle HTTP message
	 */
	// private handleError(message:any) {
	// 	// In a real world app, we might use a remote logging infrastructure
	// 	// We'd also dig deeper into the message to get a better message
	// 	let errMsg = (message.message) ? message.message :
	// 		message.status ? `${message.status} - ${message.statusText}` : 'Server message';
	// 	console.message(errMsg); // log to console instead
	// 	return Observable.throw(errMsg);
	// }
}

/*---------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------*/

class User {
	isGuest: boolean = false;
	isLoggedIn: boolean = true;
	name: string = 'vitebsk';
	role: ['editor'];
}

class App {
	pageTitle:string = '<%= APP_TITLE %>';
}

