import { Injectable } from '@angular/core';
import { Http, Response, URLSearchParams, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { LoggerService } from './logger.service/index';
import { AppService }    from './app.service/index';


@Injectable()
export class Service {
	public loaded: boolean = false;
	public name: string = 'service';
	public headers = {};

	protected baseUrl: string = this._AppService.baseURL;

	constructor (
		public _http: Http,
		public _logger: LoggerService,
		public _AppService: AppService
	) {}

	/*--------------------------------------------------------------------------------------*/
	getCurrentData(): Observable<any> {
		return this.getData(this.baseUrl+'/getCurrentData');
	}
	/*--------------------------------------------------------------------------------------*/
	toExcel(date1: string, date2: string) {
		let params = new URLSearchParams();
		params.set('date1',date1);
		params.set('date2',date2);
		return this.getData(this.baseUrl+'/toExcel', params);
	}
	/*--------------------------------------------------------------------------------------*/
	getReportData(date1: string, date2: string) {
		let params = new URLSearchParams();
		params.set('date1',date1);
		params.set('date2',date2);
		return this.getData(this.baseUrl+'/getReport', params);
	}
	/*--------------------------------------------------------------------------------------*/
	getSetup(): Observable<any> {
		return this.getData(this.baseUrl+'/setup');
	}
	saveSetup(data: any): Observable<any> {
		return this.saveData(this.baseUrl+'/setup', data);
	}
	setData(data: any): Observable<any> {
		return this.saveData(this.baseUrl+'/setData', data);
	}
	/*--------------------------------------------------------------------------------------*/
	getData (_url:string, param?:URLSearchParams): Observable<any> {
		//this.loaded = false;
		let params = new URLSearchParams();
		let headers = new Headers({ 'Content-Type': 'application/json', 'Accept':'*/*' });
		if (param) params.appendAll(param);

		return this._http.get(_url, {search: params, headers: headers})
			//.map(this.extractData)
			.map((res:Response) => {
				//console.log(res.headers.get('X-Pagination-Total-Count'));
				this.headers = res.headers;
				return res.json();
			})
			.catch(this.handleError);
	}
	/*--------------------------------------------------------------------------------------*/
	delete(id:any) {
		return this.deleteData(this.baseUrl + '/' + id);
	}
	/*--------------------------------------------------------------------------------------*/
	deleteData(_url:string): Observable<any[]> {
		let headers = new Headers({ 'Content-Type': 'application/json', 'Accept':'*/*' });
		let options = new RequestOptions({ headers: headers });

		return this._http.delete(_url, options)
			.map((res:Response) => res.json())
			.catch(this.handleError);
	}
	/*--------------------------------------------------------------------------------------*/
	save(data: any, any?:any) {
		return this.saveData(this.baseUrl, data);
	}
	/*--------------------------------------------------------------------------------------*/
	saveData(_url:string, data: any): Observable<any[]> {
		let headers = new Headers({ 'Content-Type': 'application/json', 'Accept':'*/*' });
		let options = new RequestOptions({ headers: headers });
		let body = JSON.stringify(data);

		let resp: any;

		if (data.id_port >0) {
			resp = this._http.put(_url+'/'+data.id_port, body, options);
		} else {
			resp = this._http.post(_url, body, options);
		}

		return resp.map(this.extractData).catch(this.handleError);
	}
	/*--------------------------------------------------------------------------------------*/
	addData(_url:string, data: any): Observable<any[]> {
		let headers = new Headers({ 'Content-Type': 'application/json', 'Accept':'*/*' });
		let options = new RequestOptions({ headers: headers });
		let body = JSON.stringify(data);

		let resp: any;

		resp = this._http.post(_url, body, options);

		return resp.map(this.extractData).catch(this.handleError);
	}
	/*--------------------------------------------------------------------------------------*/

	/**
	 * Handle HTTP message
	 */
	protected handleError(error:any) {
		// In a real world app, we might use a remote logging infrastructure
		// We'd also dig deeper into the message to get a better message
		let errMsg = (error.message) ? error.message :
			error.status ? `${error.status} - ${error.statusText}` : 'Возможно, отсутствует связь с сервером';
		console.error(errMsg); // log to console instead
		return Observable.throw(errMsg);


		// // In a real world app, we might send the message to remote logging infrastructure
		// let errMsg = message.message || 'Server message';
		// console.message(errMsg); // log to console instead
		// return Observable.throw(errMsg);
	}

	protected extractData(res: Response) {
		if (res.status < 200 || res.status >= 300) {
			throw new Error('Bad response status: ' + res.status);
		}
		let body = res.json();
		//console.log(' 1------> '+res.status);
		//console.debug(' 2------> ', res.headers);
		//console.log(' 3------> '+JSON.stringify(res.headers.get('X-Pagination-Total-Count')));
		//this.totalCount = 555;
		return body || { };
	}
}
