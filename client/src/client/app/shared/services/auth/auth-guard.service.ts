import { Injectable }       from '@angular/core';
import {
	CanActivate, Router,
	ActivatedRouteSnapshot,
	RouterStateSnapshot
}                        from '@angular/router';
import { AppService }    from '../app.service/app.service';
import { LoggerService } from '../logger.service/logger.service';

@Injectable()
export class AuthGuard implements CanActivate {
	constructor(
		private _AppService: AppService,
		private _logger: LoggerService,
		private router: Router
	) {}

	canActivate(
		route: ActivatedRouteSnapshot,
		state: RouterStateSnapshot
	): boolean {
		let url: string = state.url;
		return this.checkLogin(url);
	}

	checkLogin(url: string): boolean {
		if (this._AppService.user.isLoggedIn) { return true; }

			// Store the attempted URL for redirecting
		this._AppService.redirectUrl = url;

			// Navigate to the login page with extras
		this.router.navigate(['/login']);

		return false;
	}
}
