import { Component, Input } from '@angular/core';

import { AppService } from '../../services/app.service/index';
import { LoggerService } from '../../services/logger.service/index';
import { Service } from "../../services/service";

/**
 * This class represents the navigation bar component.
 */
@Component({
	moduleId: module.id,
	selector: 'sd-sensor',
	templateUrl: 'sensor.component.html',
	styleUrls: ['sensor.component.css'],
})

export class SensorComponent {
	@Input() sensor: {
		id: number,
		name: string,
		value: number,
		date: string,
		number: number,
		min_value: number,
		max_value: number,
		descr: string,
		dont_worry: boolean,
		unit: {
			id: number,
			name: string
		}
	};

	constructor(private _AppService: AppService, private _logger: LoggerService, private _servise: Service) {
	};

	/*--------------------------------------------*/
	changeBounds(port:any, val:any, step:number) {
		setTimeout(() => {
			this._servise
                .saveData(
                	this._AppService.baseURL+'/setBounds',
					{
						id_port: port.id,
						id_unit: port.unit.id,
						min_value: this.sensor.min_value,
						max_value: this.sensor.max_value,
						dont_worry: this.sensor.dont_worry
					})
                .subscribe(
					resp => {
						this._logger.debug(resp);
						this._AppService.message = new Object(null);
						this._AppService.message.text = this._logger.getTime(false)+' '+"Данные сохранены";
						this._AppService.message.color = '';
						this._AppService.snackbarOpen("Границы сохранены");
					},
					error => {
						this._logger.debug(error);
						this._AppService.message = new Object(null);
						this._AppService.message.text = this._logger.getTime(false)+' '+<any>error;
						this._AppService.message.color = 'warn';
						this._AppService.snackbarOpen("Ошибка сохранения");
					}
				);
		}, 0);
		return val+step;
	}

	saveDontWorry(sensor:any, e:any) {
		this._logger.debug(e);
		this._servise
            .saveData(
				this._AppService.baseURL+'/setBounds',
				{
					id_port: sensor.id,
					id_unit: sensor.unit.id,
					min_value: this.sensor.min_value,
					max_value: this.sensor.max_value,
					dont_worry: this.sensor.dont_worry
				})
            .subscribe(
				resp => {
					this._logger.debug(resp);
					this._AppService.snackbarOpen("Сохранено");
				},
				error => {
					this._logger.debug(error);
					this._AppService.message = new Object(null);
					this._AppService.message.text = this._logger.getTime(false)+' '+<any>error;
					this._AppService.message.color = 'warn';
					this._AppService.snackbarOpen("Ошибка сохранения");
				}
			);
	}
	checkLastDate(date: any) {
		if (!date) { return 'old'; }

		// считаем разницу с текущим временем
		let currentDate = new Date();
		let tmp = date.split(' ');
		let tmp_d = tmp[0].split('-');
		let tmp_t = tmp[1].split(':');
		let sensorDate = new Date(tmp_d[0], tmp_d[1]-1, tmp_d[2], tmp_t[0], tmp_t[1], 0, 0);
		//this._logger.debug((<any>currentDate - <any>sensorDate)/1000/60);
		if ((<any>currentDate - <any>sensorDate)/1000/60 < 10) {
			return 'ok';
		} else {
			return 'old';
		}
	}
	formatDate(date: any) {
		return date;
	}
}
