import { Component, Input } from '@angular/core';

import { AppService } from '../../services/app.service/index';
import { LoggerService } from '../../services/logger.service/index';

/**
 * This class represents the navigation bar component.
 */
@Component({
	moduleId: module.id,
	selector: 'sd-message',
	templateUrl: 'message.component.html',
	styleUrls: ['message.component.css'],
})

export class MessageComponent {
	@Input() mess: any;

	constructor(private _AppService: AppService, private _logger: LoggerService) {};

	/*--------------------------------------------*/
}
