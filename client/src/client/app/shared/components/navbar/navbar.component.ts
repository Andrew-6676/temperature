import { Component, OnInit } from '@angular/core';

import { AppService } from '../../services/app.service/index';

/**
 * This class represents the navigation bar component.
 */
@Component({
	moduleId: module.id,
	selector: 'sd-navbar',
	templateUrl: 'navbar.component.html',
	styleUrls: ['navbar.component.css'],
})

export class NavbarComponent implements OnInit {

	time: string = '-';
	constructor(private _AppService: AppService) {};

	ngOnInit(): void {
		let d = new Date();
		this.time = d.getHours()+':'+(d.getMinutes()<10 ? '0' : '')+d.getMinutes()+':'+(d.getSeconds()<10 ? '0' : '')+d.getSeconds()+', '+d.getDate()+' '+(d.getMonth()+1)+' '+d.getFullYear();

		setInterval(()=>{
			let d = new Date();
			this.time = d.getHours()+':'+(d.getMinutes()<10 ? '0' : '')+d.getMinutes()+':'+(d.getSeconds()<10 ? '0' : '')+d.getSeconds()+', '+d.getDate()+' '+(d.getMonth()+1)+' '+d.getFullYear();
		},1000);
	}
}
