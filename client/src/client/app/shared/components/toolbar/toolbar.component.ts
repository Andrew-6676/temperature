import { Component } from '@angular/core';
import { AppService } from '../../services/app.service/index';
/**
 * This class represents the toolbar component.
 */
@Component({
	moduleId: module.id,
	selector: 'sd-toolbar',
	templateUrl: 'toolbar.component.html',
	styleUrls: ['toolbar.component.css']
})

export class ToolbarComponent {
	constructor(private _AppService: AppService) {}
}


