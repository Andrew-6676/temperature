import {Component, OnInit, OnDestroy} from '@angular/core';
import { AppService } from '../shared/services/app.service/app.service';
import {Service} from "../shared/services/service";
import {LoggerService} from "../shared/services/logger.service/logger.service";

//import { SensorComponent } from '../shared/components/sensor/sensor.component';

/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {

  errorMessage: string;
  tmr: any = null;
  sensors :  sensor[] =  [];

  constructor(public _AppService: AppService,
              private _currentDataService: Service,
              private _logger: LoggerService) {}

  /**
   * Get the names OnInit
   */
  ngOnInit() {
    this.getLastData();
    // запуск запроса данных с сервера
    this.tmr = setInterval(()=>{this.getLastData()}, 30000);
  }

  ngOnDestroy(): void {
    clearInterval(this.tmr);
  }

  getLastData() {
    this._currentDataService
        .getCurrentData()
        .subscribe(
            resp => {
              this.sensors = resp.data;
              this._logger.debug(resp);
              this._AppService.message = new Object(null);
              if (resp.app_state.status=='error') {
                  this._AppService.message.color = 'warn';
                  this._AppService.message.text = this._logger.getTime(false)+' [ERROR]: '+resp.app_state.message;
                  this._AppService.snackbarOpen(" [ERROR]: "+resp.app_state.message);
              } else {
                  this._AppService.message.color = '';
                  this._AppService.snackbarOpen(" "+resp.app_state.message, 5000);
              }
            },
            error => {
              this._logger.debug(error);
              this._AppService.message = new Object(null);
              this._AppService.message.text = this._logger.getTime(false)+' '+<any>error;
              this._AppService.message.color = 'warn';
              this._AppService.snackbarOpen(" [ERROR]: "+(<any>error), 10000);
            }
        );
  }

    trackBy(index: any, item: any) {
        return false;
    }

}


class sensor {
  id: number;
  name: string;
  value: number;
  number: number;
  min_value: number;
  max_value: number;
  unit: {
    id: number;
    name: string
  }


}