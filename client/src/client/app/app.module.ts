import { NgModule }         from '@angular/core';
import { BrowserModule }    from '@angular/platform-browser';
import { APP_BASE_HREF }    from '@angular/common';
import { HttpModule }       from '@angular/http';
import { AppComponent }     from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { MaterialModule } from '@angular/material';

import { AboutModule }   from './+about/about.module';
import { HomeModule }    from './+home/home.module';
import { ReportsModule } from './+reports/reports.module';
import { SetupModule }   from './+setup/setup.module';
import { SharedModule }  from './shared/shared.module';

import { AppService }    from './shared/services/app.service/index';
import { LoggerService } from './shared/services/logger.service/index';
import { Service ,}       from "./shared/services/service";


@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    AppRoutingModule,
    AboutModule,
    HomeModule,
    ReportsModule,
    SetupModule,
    MaterialModule.forRoot(),
    SharedModule.forRoot()
  ],
  declarations: [AppComponent],
  providers: [
    AppService, LoggerService, Service,
    {
      provide: APP_BASE_HREF,
      useValue: '<%= APP_BASE %>'
    }
  ],
  bootstrap: [AppComponent]

})
export class AppModule { }
