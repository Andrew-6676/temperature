import {Component}     from '@angular/core';
import {Service}       from "../shared/services/service";
import {LoggerService} from "../shared/services/logger.service/logger.service";
import {AppService}    from "../shared/services/app.service/app.service";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';
import { Observable } from 'rxjs/Observable';
/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
    moduleId: module.id,
    selector: 'sd-setup',
    templateUrl: 'setup.component.html',
    styleUrls: ['setup.component.css']
})
export class SetupComponent {
    authorized: boolean = false;
    setup: any = {};
    setup_units: any[] = [];

    save_in_progress: boolean = false;

    date1: string;
    date2: string;
    id_port: number;
    value: number;

    request_interval: number;

    constructor(private _service: Service, private _logger: LoggerService, private _AppService: AppService) {
        this._AppService.snackbarClose();
        this._AppService.message = null;
        this.loadSetup();
    }

    loadSetup() {
        this._service
            .getSetup()
            .subscribe(
                resp => {
                    this.setup_units = [];
                    //this._logger.debug('resp:', resp.units);

                    // for (let i in resp.units) {
                    //   this.setup_units.push(resp.units[i]);
                    // }
                    this.setup = resp;
                    for (let i in this.setup.units) {
                        this.request_interval = (this.setup.units[i].request_interval)/(1000*60);
                        this._logger.debug(i, this.request_interval);
                        break;
                    }

                },
                error => {
                    console.log('err:', error);
                },
                () => {
                    this._logger.debug('loaded:', this.setup);
                }
            );
    }

    saveSetup() {
        this.save_in_progress = true;
        for (let i in this.setup.units) {
            this.setup.units[i].request_interval = this.request_interval*1000*60;
        }
        this._service
            .saveSetup(this.setup)
            .subscribe(
                resp => {
                    console.log('resp:', resp);
                    this._AppService.snackbarOpen("Сохранено");
                    this.save_in_progress = false;
                },
                error => {
                    console.log('err:', error);
                    this._AppService.snackbarOpen("[ERROR]: " + (<any>error));
                    this.save_in_progress = false;
                }
            );
    }

    setData() {
        this._service
            .setData({
                date1: this.date1,
                date2: this.date2,
                id_port: this.id_port,
                value: this.value
            })
            .subscribe(
                resp => {
                    console.log('resp:', resp);
                    this._AppService.snackbarOpen("Обновлено");
                },
                error => {
                    console.log('err:', error);
                    this._AppService.snackbarOpen("[ERROR]: " + (<any>error));
                }
            );
    }

    checkPass(pass: string) {
        // this._logger.debug(0,pass);
        // Observable.of(true)
        //     .delay(1000)
        //     .subscribe(() => {
        //         this._logger.debug(1,pass);
                if (pass=='gecnbvtyz') {
                    // this._logger.debug(3,pass);
                    this.authorized = true;
                } else {
                    this._AppService.snackbarOpen('[ERROR]: Неверный пароль', 3000);
                }
            // });

    }
}
