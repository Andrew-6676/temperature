import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetupComponent } from './setup.component';
import { SetupRoutingModule } from './setup-routing.module';
import { SharedModule } from '../shared/shared.module';

import { KeysPipe }         from '../shared/pipe/keys';

@NgModule({
  imports: [CommonModule, SetupRoutingModule, SharedModule],
  declarations: [SetupComponent, KeysPipe],
  exports: [SetupComponent]
})
export class SetupModule { }
