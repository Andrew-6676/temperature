import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportsComponent } from './reports.component';
import { ReportsRoutingModule } from './reports-routing.module';
import { SharedModule } from '../shared/shared.module';

import { ChartsModule } from 'ng2-charts/ng2-charts';

@NgModule({
  imports: [CommonModule, ReportsRoutingModule, SharedModule, ChartsModule],
  declarations: [ReportsComponent],
  exports: [ReportsComponent]
})
export class ReportsModule { }
