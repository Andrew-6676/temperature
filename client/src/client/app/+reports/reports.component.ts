import { Component }      from '@angular/core';
import { LoggerService }  from "../shared/services/logger.service/logger.service";
import { Service }        from "../shared/services/service";
import { AppService }     from "../shared/services/app.service/app.service";
import { MdIconRegistry } from "@angular/material";
import { DomSanitizer }   from "@angular/platform-browser";
import 'chart.js/dist/Chart.bundle.min.js';

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
    moduleId: module.id,
    selector: 'sd-reports',
    templateUrl: 'reports.component.html',
    styleUrls: ['reports.component.css']
})
export class ReportsComponent {
    report_data: ReportData = null;
    // units: any = {};
    ports: any = {};
    loaded: boolean = false;

    export_in_progress = false;

    date1 = '2017-01-28T23:52';
    date2 = '2017-01-31T22:52';


    constructor(
        private _logger: LoggerService,
        private _service: Service,
        private _AppService: AppService,
        iconRegistry: MdIconRegistry,
        sanitizer: DomSanitizer
    ) {
        //this.loadSetup();
        this._AppService.snackbarClose();
        this._AppService.message = null;
        let d = new Date();
        this.date1 = d.getFullYear()+'-'+(d.getMonth()+1 < 10 ? '0'+(d.getMonth()+1) : d.getMonth()+1)+'-'+(d.getDate() < 10 ? '0'+(d.getDate()) : d.getDate())+'T'+(d.getHours()-1<10 ? '0'+(d.getHours()-1) : d.getHours()-1)+':'+(d.getMinutes()<10 ? '0'+d.getMinutes() : d.getMinutes());
        this.date2 = d.getFullYear()+'-'+(d.getMonth()+1 < 10 ? '0'+(d.getMonth()+1) : d.getMonth()+1)+'-'+(d.getDate() < 10 ? '0'+(d.getDate()) : d.getDate())+'T'+(d.getHours()<10 ? '0'+d.getHours() : d.getHours())+':'+(d.getMinutes()<10 ? '0'+d.getMinutes() : d.getMinutes());
        this.loadReportData();

        iconRegistry.addSvgIcon('xls',sanitizer.bypassSecurityTrustResourceUrl('assets/svg/xls.svg'));
    }
    /*-----------------------------------*/
    toExcel() {
        this.export_in_progress = true;
        this._service
            .toExcel(this.date1, this.date2)
            .subscribe(
                res => {
                    this._logger.debug('export to', location.host+'/'+res);
                    window.open('http://'+location.host+'/'+res, '_blank');
                    this.export_in_progress = false;
                },
                err => {
                    this._logger.debug('ERROR', err);
                    this._AppService.snackbarOpen('[ERROR]: Ошибка экспорта');
                    this.export_in_progress = false;
                }
            );
    }
    /*-----------------------------------*/
    loadReportData() {
        this.loaded = false;
        this._service
            .getReportData(this.date1, this.date2)
            .subscribe(
                resp => {
                    this.report_data = new ReportData(<any>this.prepareData(resp), this);

                    //console.log('resp:', resp);
                    console.log('report_data:', this.report_data);
                    this.loaded = true;

                },
                error => {
                    console.log('err:', error);
                },
            );
    }
    /*-----------------------------------*/
    // loadSetup() {
    //     this._service
    //         .getSetup()
    //         .subscribe(
    //             resp => {
    //                 this.units = resp.units;
    //                 // this.lineChartColors = [];
    //                 // for (let u in this.units) {
    //                 //     for (let p in this.units[u].ports) {
    //                 //         this.lineChartColors.push(
    //                 //             {
    //                 //                 backgroundColor: 'rgba(0,255,0,0)',
    //                 //                 borderColor: this.units[u].ports[p].color
    //                 //             },
    //                 //         );
    //                 //     }
    //                 // }
    //                 // this._logger.debug(this.units, this.lineChartColors);
    //             },
    //         );
    // }
    /*-----------------------------------*/
    prepareData(source:any) {

        for (let p in source.ports) {
            this.ports[source.ports[p].id] = source.ports[p];
        }

        this._logger.log(this.ports);

        let res: {[key:string]:any} = {};
        // оставить показания только раз в час или два
        for (let line in source.data) {

            if (!(source.data[line].date in res)) {

                res[source.data[line].date] = {};
                for (let p in source.ports) {
                    res[source.data[line].date][source.ports[p].id] = null;
                }
            }

            if (source.data[line].date in res) {
                res[source.data[line].date][source.data[line].pid] = source.data[line].value;
            }
        }
        // this._logger.debug('preparedData_1',res);

        return res;
        // //return source;
    }
    /****************************************************/

    public lineChartOptions: any = {
        responsive: true
    };
    public lineChartColors: Array<any> = [
        {
            backgroundColor: 'rgba(125,125,125,0)',
            borderColor:'rgba(125,125,125,1)'
        },


    ];
    public lineChartLegend: boolean = true;
    public lineChartType: string = 'line';


    // events
    public chartClicked(e: any): void {
        //console.log(e);
    }

    public chartHovered(e: any): void {
        //console.log(e);
    }

    /****************************************************/
}
//
// <canvas baseChart width="400" height="400"
//     [datasets]="lineChartData"
//     [labels]="lineChartLabels"
//     [options]="lineChartOptions"
//     [colors]="lineChartColors"
//     [legend]="lineChartLegend"
//     [chartType]="lineChartType"
// (chartHover)="chartHovered($event)"
// (chartClick)="chartClicked($event)"></canvas>

export class ReportData {
    data: {data: number[], label: string}[] = [];
    labels: any[] = [];
    colors: {
        backgroundColor: string,
        borderColor: string
    }[] = [];

    constructor(rawData: any[], parent: any) {
    //     console.debug('rawData', rawData);
        console.debug('preparedData', rawData);
    //     // преобразуем полученные данные для построения графика
    //     let tmpData: {[key: string]: any[]} = {};
    //     for (let row of rawData) {
    //         if (this.labels.indexOf(row.date)<0) {
    //            this.labels.push(row.date);
    //         }
    //         if (!tmpData[row.pname]) {
    //             tmpData[row.pname] = [];
    //         }
    //         tmpData[row.pname].push(row.value);
    //     }
    //
    //     for (let l in tmpData) {
    //         this.data.push({data: tmpData[l], label: l});
    //     }
        let tmp_data : {[key:number]: any[]} = {};
        for (let dd in rawData) {
            this.labels.push(dd);
            for (let pp in rawData[dd]) {
                if (!tmp_data[<any>pp]) {
                    tmp_data[<any>pp] = [];
                }
                tmp_data[<any>pp].push(rawData[dd][pp]);
            }
        }

        // console.debug('labels', this.labels);
        console.debug('data', tmp_data);

        for (let td in tmp_data) {
            this.data.push({data: tmp_data[td], label: parent.ports[td].name});
        }

        for (let p in parent.ports) {
            this.colors.push(
                {
                    borderColor: parent.ports[p].color,
                    backgroundColor: 'rgba(125,125,125,0)'
                }
            );
        }

    }
}