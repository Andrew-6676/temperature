import { Component }  from '@angular/core';
import { Config }     from './shared/config/env.config';
import './operators';
import { AppService } from "./shared/services/app.service/app.service";

import 'hammerjs/hammer.js'
/**
 * This class represents the main application component.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-app',
  templateUrl: 'app.component.html',
})
export class AppComponent {
  constructor(public _AppService: AppService) {
    console.log('Environment config', Config);
  }
}
