function saveSetup(req, res) {
    console.log(utils.getTime()+'[CLIENT]: save setup');
    res.setHeader("Access-Control-Allow-Origin", '*');
    res.setHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers");

    let sqls = [];
    for (let u in req.body.units) {
        sqls.push(
            {
                sql:'update unit set address=?, name=?, serial_port=?, request_interval=?, enable=?, description=? where id=?',
                params:[
                    req.body.units[u].address,
                    req.body.units[u].name,
                    req.body.units[u].serial_port,
                    req.body.units[u].request_interval,
                    req.body.units[u].enable,
                    req.body.units[u].description,
                    req.body.units[u].id,
                ]
            }
        );
        for (p in req.body.units[u].ports) {
            sqls.push(
                {
                    sql: 'update port set name=?, number=?, min_value=?, max_value=?, enable=?, description=?, color=? where id=?',
                    params: [
                        req.body.units[u].ports[p].name,
                        req.body.units[u].ports[p].number,
                        req.body.units[u].ports[p].min_value,
                        req.body.units[u].ports[p].max_value,
                        req.body.units[u].ports[p].enable,
                        req.body.units[u].ports[p].description,
                        req.body.units[u].ports[p].color,
                        req.body.units[u].ports[p].id
                    ]
                }
            );
        }
    }

    for (let s of sqls) {
        database.serialize(function () {
            database.run(s.sql, s.params, function (err) {
                if (err) console.log(err);
            });
            //console.log(s.sql);
        });
    }

    setup = req.body;
    modbus.stop();
    setTimeout(()=>{modbus.start(setup)}, 1000);

    res.json(
        {
            sql: sqls,
            setup: setup,
            body: req.body
        }
    );
}


module.exports = saveSetup;