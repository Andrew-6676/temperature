'use strict';

function setBounds(req, res) {
    console.log('[CLIENT] setBounds', req.params);
    res.setHeader("Access-Control-Allow-Origin", '*');
    res.setHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers");

    database.run("update port set min_value=?, max_value=?, dont_worry=? where id=?", [req.body.min_value, req.body.max_value, req.body.dont_worry, req.params.id_port], function (err) {
        if (err) {
            console.log(utils.getTime() + '[DB]: update bounds error', err);
            res.json(
                {
                    status: 'error',
                    message: "Ошибка сохранения параметров!",
                    ereror: err
                }
            );
        } else {
            console.log(utils.getTime() + '[DB]: update bounds success');
                // приводим в соответствие объект с настройками
            setup.units[req.body.id_unit].ports[req.body.id_port].min_value = req.body.min_value;
            setup.units[req.body.id_unit].ports[req.body.id_port].max_value = req.body.max_value;
            //console.log(setup.units[req.body.id_unit].ports[req.body.id_port]);
            res.json(
                {
                    status: 'ok',
                    message: "Сохранено успешно",
                    id_port: req.params.id_port,
                    body: req.body
                }
            );
        }
    });
}

module.exports = setBounds;