// берём Express
express = require('express');
server = require('express')();
let bodyParser = require('body-parser');
let compression = require('compression');

server.use(bodyParser.json());
server.use(express.static(__dirname+'/../client'));
server.use(express.static(__dirname+'/../reports'));
server.use(compression());

let router = new Object(null);
router.setBounds = require('./setBounds');
router.getReport = require('./getReport');
router.saveSetup = require('./saveSetup');
router.setData   = require('./setData');
router.toExcel   = require('./toExcel');
router.getCurrentData = require('./getCurrentData');


// создаём Express-приложение
//let app = express();

// создаём маршрут для главной страницы
// http://localhost:8080/


server.options('/', function (req, res) {
    res.setHeader("Access-Control-Allow-Origin", '*');
    res.setHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers");
    res.setHeader("Access-Control-Allow-Methods", "GET");
    res.json(['get']);
});

    // как-то надо отдать index.html
server.get('/', function (req, res) {
    console.log(utils.getTime() + ' getClientPage');
    res.sendFile('index.html');
});
/*------------------------------------------------------------------------*/

server.options('/backend/setBounds/:id_port', function (req, res) {
    res.setHeader("Access-Control-Allow-Origin", '*');
    res.setHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers");
    res.setHeader("Access-Control-Allow-Methods", "PUT");
    res.json(['put']);
});
//console.log(router.setBounds);
server.put('/backend/setBounds/:id_port', router.setBounds);
/*------------------------------------------------------------------------*/

server.options('/backend/setup', function (req, res) {
    res.setHeader("Access-Control-Allow-Origin", '*');
    res.setHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers");
    res.json(['head','get', 'post']);
});
server.get('/backend/setup', function (req, res) {
    console.log(utils.getTime()+'[CLIENT]: load setup');
    let data = null;

    res.setHeader("Access-Control-Allow-Origin", '*');
    res.setHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers");
    res.json(setup);
});
server.post('/backend/setup', router.saveSetup);
/*------------------------------------------------------------------------*/
server.options('/backend/getReport', function (req, res) {
    res.setHeader("Access-Control-Allow-Origin", '*');
    res.setHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers");
    res.json(['head','get']);
});
server.get('/backend/getReport', router.getReport);
/*------------------------------------------------------------------------*/
server.options('/backend/getCurrentData', function (req, res) {
    res.setHeader("Access-Control-Allow-Origin", '*');
    res.setHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers");
    res.json(['head','get']);
});

server.get('/backend/getCurrentData', router.getCurrentData);

/*------------------------------------------------------------------------*/
server.options('/backend/setData/:id', function (req, res) {
    res.setHeader("Access-Control-Allow-Origin", '*');
    res.setHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers");
    res.setHeader("Access-Control-Allow-Methods", "PUT");
    res.json(['put']);
});

server.put('/backend/setData/:id', router.setData);
/*------------------------------------------------------------------------*/
server.options('/backend/toExcel', function (req, res) {
    res.setHeader("Access-Control-Allow-Origin", '*');
    res.setHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers");
    res.json(['get']);
});

server.get('/backend/toExcel', router.toExcel);
/*------------------------------------------------------------------------*/

module.exports = server;