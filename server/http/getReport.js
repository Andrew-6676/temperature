'use strict';
let promise;
let ports;

function getReport(req, res) {
    console.log(utils.getTime()+'[CLIENT]: getReport('+req.query.date1+','+req.query.date2+')');
    res.setHeader("Access-Control-Allow-Origin", '*');
    res.setHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers");

    let t1 = req.query.date1.split('T');
    let t2 = req.query.date2.split('T');

    let date1 = t1.join(' ');
    let date2 = t2.join(' ');

    console.log(date1, date2);

    let data = null;

    promise = new Promise((resolve, reject) => {
    database.all('select * from port where id in (select distinct p.id ' +
        ' from temperature t' +
        ' inner join port p on p.id=t.id_port' +
        ' inner join unit u on u.id=p.id_unit' +
        ' where u.enable>0 and p.enable>0 and datetime(date, \'+3 hour\')>\''+date1+'\' and datetime(date, \'+3 hour\')<\''+date2+'\'' +
        ' order by date )', function (err, rows) {
            if (err) {
                console.log(utils.getTime() + ' Ошибка получения отчёта из БД:', err);
                reject(false);
            } else {
                ports = rows;
                resolve(rows);
            }
        });
    });

    promise.then(
        resp => {
            database.all('select t.id, u.id as uid, p.id as pid, p.name as pname, datetime(date, \'+3 hour\') as date, value, request_interval ' +
                ' from temperature t' +
                ' inner join port p on p.id=t.id_port' +
                ' inner join unit u on u.id=p.id_unit' +
                ' where u.enable>0 and p.enable>0 and datetime(date, \'+3 hour\')>\''+date1+'\' and datetime(date, \'+3 hour\')<\''+date2+'\'' +
                ' order by date ', {},
                function (err, rows) {
                    if (err) {
                        console.log(utils.getTime() + ' Ошибка получения отчёта из БД:', err);
                        return 1;
                    } else {
                        console.log(utils.getTime() + '[DB]: get report data success');
                        //console.log(rows);
                        //data = rows;
                        //let arr = rows;
                        res.json({ports: resp, data: rows});
                    }
                }
            );
        }
    );

}

module.exports = getReport;