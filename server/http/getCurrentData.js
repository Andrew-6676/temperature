function getCurrentData(req, res) {
    console.log(utils.getTime()+'[CLIENT]: getCurrentData');
    res.setHeader("Access-Control-Allow-Origin", '*');
    res.setHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers");

    let data = null;
    database.all('select p.* , u.name as uname, datetime(t.date, \'+3 hour\') as date, t.value ' +
        'from port p ' +
        'inner join unit u on p.id_unit=u.id ' +
        'left join temperature t on t.id_port = p.id ' +
        'where p.enable=1 ' +
        'group by p.id', {},
        function (err, rows) {
            if (err) {
                console.log(utils.getTime() + ' Ошибка получения данных из БД:', err);
                return 1;
            } else {
                console.log(utils.getTime() + '[DB]: get current data success');
                //console.log(rows);
                //data = rows;
                let arr = [];
                for (let d of rows) {
                    //console.log(d);
                    arr.push(
                        {
                            id: d.id,
                            name: d.name,
                            value: d.value,
                            date: d.date,
                            number: d.number,
                            min_value: d.min_value,
                            max_value: d.max_value,
                            descr: d.description,
                            dont_worry: d.dont_worry,
                            color: d.color,
                            unit: {
                                id: d.id_unit,
                                name: d.uname
                            }
                        }
                    );
                }
                //console.log(arr);
                res.json({data: arr, app_state: app_state});
            }
        }
    );
    //console.log(req.get('Origin'));

}


module.exports = getCurrentData;