'use strict';
/*---------------------------------------------------------------*/
function toExcel(req, res) {
    console.log(utils.getTime() + '[CLIENT]: toExcel(' + req.query.date1 + ',' + req.query.date2 + ')');
    res.setHeader("Access-Control-Allow-Origin", '*');
    res.setHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers");

    let t1 = req.query.date1.split('T');
    let t2 = req.query.date2.split('T');

    let date1 = t1.join(' ');
    let date2 = t2.join(' ');

    let ports = {};
    let data = {};
    let promise;
    let f_name = 'reports/report.xlsx';
    database.all('select * from port where enable=1', function (err, rows) {
        promise = new Promise((resolve, reject) => {
            if (err) {
                console.log(utils.getTime() + ' Ошибка получения отчёта из БД:', err);
                resolve(false);
            } else {
                ports = rows;
                resolve(true);
            }
        });
    });
    database.all('select t.id, p.id as pid, p.name as pname, datetime(date, \'+3 hour\') as date, value, color  ' +
        ' from temperature t' +
        ' inner join port p on p.id=t.id_port' +
        ' inner join unit u on u.id=p.id_unit' +
        ' where u.enable>0 and p.enable>0 and datetime(date, \'+3 hour\')>\'' + date1 + '\' and datetime(date, \'+3 hour\')<\'' + date2 + '\'' +
        ' order by date ', {},
        function (err, rows) {
            if (err) {
                console.log(utils.getTime() + ' Ошибка получения отчёта из БД:', err);
                return 1;
            } else {
                data = rows;
                promise.then(
                    result => {
                        makeExcel({ports: ports, data: data}, f_name).then(
                            result => {
                                    // отдаём результат экспорта клиенту
                                //server.use(express.static(__dirname+'/../reports'));
                                //res.sendFile(__dirname+'/../reports/'+f_name);
                                res.json(f_name.split('/')[1]);
                            }
                        );
                    },
                    error => {
                        res.json('error');
                    }
                );
            }
        }
    );
}
/*---------------------------------------------------------------*/
function makeExcel(source, f_name) {
    let prepared_data = prepareData(source);

    let Excel = require('exceljs');
    //по столбцам - датчики, по строкам время через час.
    let workbook = new Excel.Workbook();
    let worksheet = workbook.addWorksheet('Отчёт');
    //worksheet.getCell('C3').value = new Date(1968, 5, 1);
    worksheet.getColumn(1).width = 19;




    let c = 2;
    let current_line = 1;
        // строка с датчиками
    let row = worksheet.getRow(current_line++);
    for (let p in source.ports) {
        row.getCell(c).value = source.ports[p].name;
        row.getCell(c).border = {
            top: {style:'thin'},
            left: {style:'thin'},
            bottom: {style:'thin'},
            right: {style:'thin'}
        };
        row.getCell(c).alignment = { vertical: 'justify', horizontal: 'center' };
        c++;
    }

    // вывод дат с температурами
    for (let key in prepared_data) {
        row = worksheet.getRow(current_line++);
        row.getCell(1).value = key;
        row.getCell(1).border = {
            top: {style:'thin'},
            left: {style:'thin'},
            bottom: {style:'thin'},
            right: {style:'thin'}
        };

        let c = 2;
        for (let pid in prepared_data[key]) {
            row.getCell(c).value = prepared_data[key][pid] ? prepared_data[key][pid] : '';
            row.getCell(c).border = {
                top: {style:'thin'},
                left: {style:'thin'},
                bottom: {style:'thin'},
                right: {style:'thin'}
            };
            row.getCell(c).alignment = { vertical: 'middle', horizontal: 'right' };
            c++;
        }

    }

    // write to a file

    return new Promise((resolve, reject) => {
        workbook.xlsx.writeFile(f_name)
            .then(function() {
                //  res.json({status: 'saved', data:rows});
                resolve('saved to file '+f_name+'!');
            });
    });

}
/*---------------------------------------------------------------*/
function prepareData(source) {
    //console.log(source);
    let res = {};
        // оставить показания только раз в час или два
    let checkpoint = new Date(0);
    for (let line in source.data) {
        let new_date = false;
        if (checkpoint<(new Date(source.data[line].date))) {
            let dd = new Date(source.data[line].date);
            checkpoint = dd.setHours(dd.getHours()+1);
            //console.log(new Date(checkpoint));
            new_date = true;
        }

        if (new_date && !(source.data[line].date in res)) {

            res[source.data[line].date] = {};
            for (let p in source.ports) {
                res[source.data[line].date][source.ports[p].id] = null;
            }
        }

        if (source.data[line].date in res) {
            res[source.data[line].date][source.data[line].pid] = source.data[line].value;
        }
    }

    //console.log(res);
    return res;
    //return source;
}

module.exports = toExcel;