function setData(req, res) {
    console.log(utils.getTime()+'[CLIENT]: getCurrentData');
    res.setHeader("Access-Control-Allow-Origin", '*');
    res.setHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers");

    let data = null;
    database.run('update temperature' +
        ' set value=?' +
        ' where id_port=? and `date`>? and `date`<?', [req.body.value, req.body.id_port, req.body.date1.split('T').join(' '), req.body.date2.split('T').join(' ')],
        function (err, rows) {
            if (err) {
                console.log(utils.getTime() + ' Ошибка изменениия данных в БД:', err);
                return 1;
            } else {
                console.log(utils.getTime() + '[DB]: set data success');
                //console.log(rows);
                //data = rows;

                //console.log(arr);
                res.json(req.body);
            }
        }
    );
    //console.log(req.get('Origin'));

}


module.exports = setData;