let ModbusRTU = require("modbus-serial");


let promise;
let to = [];

let g_setup;
let closed = true;

function start(setup) {
    g_setup = setup;
    console.log(utils.getTime() + " Подготовка к сбору температуры....");
    for (let u in setup.units) {
        // конектимся к контроллеру
        connect(setup.units[u]);
        break;
    }

}

function stop() {
    for (let t of to) {
        clearTimeout(t);
    }
    if (!closed && clientModbus != null) {
        clientModbus.close(() => {
            console.log('Port closed. Waiting 10 s to reconect...');
            closed=true;
        });
        clientModbus = null;
    }
}

function connect(unit) {
    console.log(utils.getTime() + " Подключение к порту \"" + unit.serial_port + "\"; interval=" + unit.request_interval);
    clientModbus = new ModbusRTU();
    clientModbus.connectRTU(unit.serial_port, {baudrate: 9600}, function() {
        closed = false;
        for (let u in g_setup.units) {
            if (g_setup.units[u].enable>0) {
                setTimeout(
                    ()=>{
                        polling(g_setup.units[u], g_setup.ports[u]);
                    }, g_setup.units[u].address*500
                );
            }
        }
    });
}

function polling(unit, ports) {
    if (closed) { return; }

    if (clientModbus != null) {
        clientModbus.setID(unit.address);
        clientModbus.readHoldingRegisters(0, 8, function (err, data) {
            if (err) {
                //console.log(data);
                app_state.status = 'error';
                app_state.message = err;
                console.log(utils.getTime() + "[SENSORS]: Опрос \"" + unit.name + "\":" + ' ОШИБКА получения данных с устройства [' + unit.name + ', ' + unit.serial_port + ', ' + unit.address + ']:', err);
                // если ошибка - переподключаемся через время
                stop();
                to[unit.id] = setTimeout(
                    () => {
                        connect(unit);
                    },
                    10000
                );
            } else {
                app_state.status = 'ok';
                app_state.message = 'Данные получены.';
                //console.log(data.data);
                /*
                 * 65535 > T > 10000 - отрицательная температура t= -(T-10000)/10
                 * 10000 > T > 0 - положительная температура t= T/10
                 * */
                let temp = {};
                for (let t in data.data) {
                    if (data.data[t] != 65535) {
                        // console.log(data.data[t]);
                        temp[t] = {
                            id_port: ports[t],
                            value: (data.data[t] < 10000 ? data.data[t] / 10 : (-(data.data[t] - 10000) / 10))
                        };
                    }
                }

                console.log(utils.getTime() + "[SENSORS]: Опрос \"" + unit.name + "\":\n", temp);
                for (let t in temp) {
                    //console.log(temp[t].id_port);
                    database.run("insert into temperature (id_port, date, value) values(?,strftime('%Y-%m-%d %H:%M:00'),?)", [temp[t].id_port, temp[t].value], function (err) {
                        if (err) {
                            // console.log(err);
                            console.log(utils.getTime() + '[DB-ERROR]: insert temperature error', err);
                        } else {
                            //console.log(this);
                            console.log(utils.getTime() + '[DB]: insert temperature succes');
                        }
                    })
                }
                // планируем следующий опрос
                to[unit.id] = setTimeout(
                    () => {
                        polling(unit, ports)
                    },
                    unit.request_interval
                );
            }
        });
    }

}


module.exports.start = start;
module.exports.stop = stop;