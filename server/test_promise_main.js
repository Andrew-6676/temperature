/**
 * Created by andrew-6676 on 18.01.2017.
 */
let data = require('./test_promise');

data.then(
    result => {
        // первая функция-обработчик - запустится при вызове resolve
        res = result;
        console.log("3 Fulfilled: " + res); // result - аргумент resolve
    },
    error => {
        // вторая функция - запустится при вызове reject
        console.log("Rejected: " + error); // message - аргумент reject
    });