'use strict';

let query_results = {};
let utils = require('./utils.js');
let sqlite3 = require('sqlite3').verbose();

let promise;


console.log('waiting for database...');


let db = new sqlite3.Database('./db/data.db', sqlite3.OPEN_READWRITE);

//function openDB(err) {
//   if (err) {
//        console.log(utils.getTime() + ' Ошибка открытия БД:', err);
//    } else {
//        console.log(utils.getTime() + ' БД открыта!');
        db.serialize(function () {
            db.all("select * from unit", getUnits);
            db.all("select * from port order by id_unit, number", getPorts);
        });
//    }
//}

promise = new Promise((resolve, reject) => {
    db.close(function (err) {
        //console.log(query_results);
        resolve(query_results);
    });
});

/*----------------------------------------------*/
function getUnits(err, rows) {
    if (err) {
        console.log(utils.getTime()+' Ошибка получения списка устройств из БД:', err);
        query_results.units = false
    } else {
        query_results.units = rows;
    }
}

function getPorts (err, rows) {
    if (err) {
        console.log(utils.getTime()+' Ошибка получения списка датчиков из БД:', err);
        query_results.ports = false;
    } else {
        query_results.ports = rows;
        //console.log(rows);
    }
}
/*-------------------------------------*/

module.exports.getSetup = promise;