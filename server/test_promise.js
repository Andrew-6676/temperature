/**
 * Created by andrew-6676 on 18.01.2017.
 */
'use strict';
let res = '-';
// Создаётся объект promise
let promise = new Promise((resolve, reject) => {

    setTimeout(() => {
        // переведёт промис в состояние fulfilled с результатом "result"
        resolve("promise result");
    }, 2000);

});

// promise.then навешивает обработчики на успешный результат или ошибку
// promise
//     .then(
//         result => {
//             // первая функция-обработчик - запустится при вызове resolve
//             res = result;
//             //console.log("Fulfilled: " + res); // result - аргумент resolve
//         },
//         message => {
//             // вторая функция - запустится при вызове reject
//             console.log("Rejected: " + message); // message - аргумент reject
//         }
//    );
console.log('waiting for database...');

module.exports = promise.then(
    result => {
        // первая функция-обработчик - запустится при вызове resolve
        res = result+'_1';
        //console.log("1 Fulfilled: ", res); // result - аргумент resolve
        return res;
    },
    error => {
        // вторая функция - запустится при вызове reject
        //console.log("Rejected: " + message); // message - аргумент reject
    }).then(
        result => {
            res = result+'_2';
            //console.log("2 Fulfilled: ", res); // result - аргумент resolve
            return new Promise((resolve, reject) => {
                resolve(res);
            });
        }
);

//module.exports = promise;