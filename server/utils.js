/**
 * Created by andrew-6676 on 18.01.2017.
 */


module.exports.getTime = function()  {
    var d = new Date();
    return '['+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds()+'.'+d.getMilliseconds()+']';
};