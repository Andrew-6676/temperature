let db = require('./db.class');
let sqlite3 = require('sqlite3').verbose();

utils = require('./utils');
database = new sqlite3.Database('./db/data.db', sqlite3.OPEN_READWRITE);
setup = {};
lastData = [];
modbus = require('./modbus');

app_state = {
    status: 'ok',
    message: null
};

// let ModbusRTU = require("modbus-serial");
// let clientModbus = new ModbusRTU();

to = {};    // для хранения setTimeout, при сохранении настроек надо перезапустить отпрос датчиков с новыми настройками
    // для обработки запросов от клиента
let http_server = require('./http/http_server');

/**************************************************************************/
// ждём из БД настроек
db.getSetup.then(
    result => {
        // формируем удобный объект с настройками
        setup = {units: {}, ports: {}};
        for (let u of result.units) {
            // console.log(u);
            setup.units[u.id] = u;
            setup.units[u.id].ports = {};
            setup.ports[u.id] = {}
        }

        for (let p of result.ports) {
            //console.log(p);
            setup.units[p.id_unit].ports[p.id] = p;
            setup.ports[p.id_unit][p.number] = p.id;
        }
        //console.log(utils.getTime()+" setup from db: ", res); // result - аргумент resolve
        return setup;
    },
    error => {
        //console.log(utils.getTime()+"Rejected: ", message); // message - аргумент reject
    }).then(
    result => {
        console.log(utils.getTime() + " Настройки загружены из БД.");
        // console.log(utils.getTime()+" normalized setup: ", setup.units);
        // запускаем опрос контроллеров для каждого активного устройства
        modbus.start(setup);
        return true;
    },
    reject => {
        console.log(reject);
    }
).then(
    result => {
        // запускаем web-сервер на порту 8080
        http_server.listen(8888);
        // отправляем сообщение
        console.log(utils.getTime() + ' Web-сервер стартовал!');
    }
);



/*-------------------------------------------------------------------*/
/*-------------------------------------------------------------------*/

